
export default function ExpenseTable(props: any){

    const expenses = props.expenses;

    return(<table className="table">
        <thead><th>Expense ID</th><th>Wedding ID</th><th>Reason</th><th>Amount</th></thead>
        <tbody>
            {expenses.map(b => <tr key={b.expenseId}>
                <td>{b.expenseId}</td>
                <td>{b.weddingId}</td>
                <td>{b.reason}</td>
                <td>{b.amount}</td>
            </tr>)}
        </tbody>
    </table>)
}