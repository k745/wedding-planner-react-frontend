
export default function Home(props: {fName:string, lName:string, isLoggedIn:boolean}){
    
    function Planner(event){
        window.location.href = '/planner'
    }
    function Message(event){
        window.location.href = '/message'
    }

    return(<div>
        <h1>Home</h1>
        <button onClick={Planner}>Planner</button>
        <button onClick={Message}>Messaging</button>
    </div>)
}