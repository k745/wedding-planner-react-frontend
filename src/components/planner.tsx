import { SyntheticEvent, useRef, useState } from "react";
import axios from 'axios'
import WeddingTable from "./wedding-table";
import ExpenseTable from "./expense-table";

export default function Planner(){

    const weddingIdInput = useRef<any>();
    const weddingDateInput = useRef<any>();
    const weddingLocationInput = useRef<any>();
    const weddingNameInput = useRef<any>();
    const weddingBudgetInput = useRef<any>();


    const expenseIdInput = useRef<any>();
    const weddingIdInput2 = useRef<any>();
    const expenseReasonInput = useRef<any>();
    const expenseAmountInput = useRef<any>();

    const [weddings,setWeddings] = useState([]);
    const [expenses,setExpenses] = useState([]);
    const [file,setFile] = useState(null)

    async function performWeddingAction(event:SyntheticEvent){

        var action = ((document.getElementById("action")) as HTMLSelectElement).value;

        const id = weddingIdInput.current.value;
        const date = weddingDateInput.current.value;
        const location = weddingLocationInput.current.value;
        const name = weddingNameInput.current.value;
        const budget = weddingBudgetInput.current.value;
        const data = {
            weddingId: id,
            date: date,
            location: location,
            name: name,
            budget: budget
        }
        const json = JSON.stringify(data);

        if(action === "view"){
            
            if(id){
                const res = await axios.get(`http://34.122.255.185:3000/weddings/${id}`);
                setWeddings([res.data]);
                return;
            } else {
                const res = await axios.get('http://34.122.255.185:3000/weddings');
                setWeddings(res.data);
                return;
            }

        } else if(action === "edit"){

            if(!id||!date||!location||!name||!budget){
                return alert("Please ensure all fields are filled out!");
            }
            const res = await axios.put('http://34.122.255.185:3000/weddings', json, {
                headers: { 
                    'Content-Type': 'application/json'
                }
            });
            setWeddings([res.data]);
            return;

        } else if(action === "create"){

            if(!id||!date||!location||!name||!budget){
                return alert("Please ensure all fields are filled out!");
            }
            const res = await axios.post('http://34.122.255.185:3000/weddings', json, {
                headers: { 
                    'Content-Type': 'application/json'
                }
            });  
            setWeddings([res.data])
            return;

        } else if(action === "delete"){
            if(!id){
                return alert("Please ensure wedding ID field is filled out!");
            }
            const res = await axios.delete(`http://34.122.255.185:3000/weddings/${id}`)
            if(res){
                return alert("Wedding deleted")
            }
        } else {
            return alert("Unrecognized action. Please refresh the page or try again later.")
        }
    }

    async function performExpenseAction(event:SyntheticEvent){

        var actionE = ((document.getElementById("actionE")) as HTMLSelectElement).value;
        const eId = expenseIdInput.current.value;
        const id = weddingIdInput2.current.value;
        const reason = expenseReasonInput.current.value;
        const amount = expenseAmountInput.current.value;
        const data = {
            expenseId: eId,
            weddingId: id,
            reason: reason,
            amount: amount
        }
        const json = JSON.stringify(data);

        if(actionE === "view"){
            
            if(id){
                const res = await axios.get(`http://34.122.255.185:3000/weddings/${id}/expenses`);
                setExpenses(res.data);
                return;
            }
            else if(eId){
                const res = await axios.get(`http://34.122.255.185:3000/expenses/${eId}`);
                setExpenses([res.data]);
                return;
            } else {
                const res = await axios.get('http://34.122.255.185:3000/expenses');
                setExpenses(res.data);
                return;
            }

        } else if(actionE === "edit"){

            if(!eId||!id||!reason||!amount){
                return alert("Please ensure all fields are filled out!");
            }
            const res = await axios.put('http://34.122.255.185:3000/expenses', json, {
                headers: { 
                    'Content-Type': 'application/json'
                }
            });
            setExpenses([res.data]);
            return;

        } else if(actionE === "create"){

            if(!eId||!id||!reason||!amount){
                return alert("Please ensure all fields are filled out!");
            }
            if(file){
                var fullPath = (document.getElementById('fileUpload') as HTMLInputElement).value;
                if (fullPath) {
                    var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                    var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }
                var fileExt = filename.split('.').pop();
                filename = filename.split('.').slice(0, -1).join('.')
            }
                const fileSize = (document.getElementById('fileUpload') as HTMLInputElement).files[0];
                if(fileSize.size > 10485760){
                    alert("Uploaded file must be less than 10 mb!")
                    return;
                }
                const fileContent = await toBase64(file);
                const fileData = {
                    name: filename,
                    extension: fileExt,
                    content: fileContent
                }
                const json = JSON.stringify(fileData);

                  const res = await axios.post(`http://us-central1-wedding-planner-kirk.cloudfunctions.net/uploadphoto`, json, {
                  headers: { 
                    'Content-Type': 'application/json'
                }
                });
                  const photoURL = res.data.photoLink;
                  alert(`Photo successfully uploaded to ${photoURL}`)
            } else {
                alert("Please upload a photo for reimbursment purposes!")
                return;
            }
            const res = await axios.post('http://34.122.255.185:3000/expenses', json, {
                headers: { 
                    'Content-Type': 'application/json'
                }
            });  

            setExpenses([res.data])
            return;

        } else if(actionE === "delete"){
            if(!eId){
                return alert("Please ensure expense ID field is filled out!");
            }
            const res = await axios.delete(`http://34.122.255.185:3000/expenses/${eId}`)
            if(res){
                return alert("expense deleted")
            }
        } else {
            return alert("Unrecognized action. Please refresh the page or try again later.")
        }
    }

    function toBase64(file){
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
              let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
              if ((encoded.length % 4) > 0) {
                encoded += '='.repeat(4 - (encoded.length % 4));
              }
              resolve(encoded);
            };
            reader.onerror = error => reject(error);
          });
      }

      function onFileChange(file){
        setFile(file);
      }

      function returnToHome(event){
        window.location.href = '/home'
    }

    return(<div>
        <button className="left-align" onClick={returnToHome}>Return to Home</button>
        <h2 color="rgb(165, 42, 42)">Wedding Planner</h2>
        <input placeholder="Wedding ID:" ref={weddingIdInput}></input>
        <input placeholder="Wedding Date YYYY-MM-DD:" ref={weddingDateInput}></input> 
        <input placeholder="Wedding Location:" ref={weddingLocationInput}></input> 
        <input placeholder="Wedding Name:" ref={weddingNameInput}></input> 
        <input placeholder="Wedding Budget:" ref={weddingBudgetInput}></input> 
        <select id="action">
            <option value="create">Create Wedding</option>
            <option value="edit">Edit Wedding</option>
            <option value="delete">Delete Wedding</option>
            <option value="view">View Weddings</option>
        </select>
        <button type="button" onClick={performWeddingAction}>Submit</button>
        <WeddingTable weddings={weddings}></WeddingTable>
        <br></br>
        <input placeholder="Expense ID:" ref={expenseIdInput}></input>
        <input placeholder="Wedding ID" ref={weddingIdInput2}></input> 
        <input placeholder="Reason" ref={expenseReasonInput}></input> 
        <input placeholder="Amount" ref={expenseAmountInput}></input> 
        <select id="actionE">
            <option value="create">Create Expense</option>
            <option value="edit">Edit Expenses</option>
            <option value="delete">Delete Expense</option>
            <option value="view">View Expenses</option>
        </select>
        <input type="file" accept="image/png, image/jpg" id="fileUpload" name="fileUpload" onChange={(e) => onFileChange(e.target.files[0])}></input>
        <button type="button" onClick={performExpenseAction}>Submit</button>
        <ExpenseTable expenses={expenses}></ExpenseTable>

    </div>)

}