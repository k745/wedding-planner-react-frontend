
export default function WeddingTable(props: any){

    const weddings = props.weddings;

    return(<table className="table">
        <thead><th>Wedding ID</th><th>Date</th><th>Location</th><th>Name</th><th>Budget</th></thead>
        <tbody>
            {weddings.map(a => <tr key={a.weddingId}>
                <td>{a.weddingId}</td>
                <td>{a.date}</td>
                <td>{a.location}</td>
                <td>{a.name}</td>
                <td>{a.budget}</td>
            </tr>)}
        </tbody>
    </table>)
}