import axios from "axios";
import { SyntheticEvent, useRef } from "react";

export default function Login(){

    const emailInput = useRef(null);
    const passwordInput = useRef(null);

    async function submit(event:SyntheticEvent){
    const email = emailInput.current.value;
    const password = passwordInput.current.value;

    const data = {
        email: email,
        password: password
    }
    const json = JSON.stringify(data)
    try{
        const res = await axios.patch(`https://authorization-dot-wedding-planner-kirk.ue.r.appspot.com/users/login`, json, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        alert(`Successfully logged in as ${res.data.fName} ${res.data.lName}`)
        window.location.href='/home';
    } catch {
        alert('Login attemt unsuccessful. Please try again')
        return;
    }
}
    return(<div>
        <h1>Login</h1>
        <input placeholder="email" ref={emailInput}></input>
        <input placeholder="password" ref={passwordInput}></input>
        <button onClick={submit}>Submit</button>
    </div>)
}