import axios from "axios";
import { SyntheticEvent, useRef, useState } from "react";
import MessageTable from "./message-table";

export default function Messaging(){

    const noteInput = useRef(null);
    const senderInput = useRef(null);
    const recipientInput = useRef(null);
    const senderInput2 = useRef(null);
    const recipientInput2 = useRef(null);
    const [messages,setMessages] = useState([]);


    function searchView(event:SyntheticEvent){
        document.getElementById("search").style.display = "block";
        document.getElementById("selection").style.display = "none";
        document.getElementById("selection2").style.display = "none";

    }
    function composeView(event:SyntheticEvent){
        document.getElementById("compose").style.display = "block";
        document.getElementById("selection").style.display = "none";
        document.getElementById("selection2").style.display = "none";
    }
    function back(event:SyntheticEvent){
        window.location.href='/message';
    }

    async function search(event:SyntheticEvent){
        const sender = senderInput.current.value;
        const recipient = recipientInput.current.value;
        if(senderInput.current.value && recipientInput.current.value){
            const res = await axios.get(`http://wedding-planner-kirk.ue.r.appspot.com/messages?sender=${sender}&recipient=${recipient}`)
            setMessages(res.data)
        } else if(senderInput.current.value){
            const res = await axios.get(`http://wedding-planner-kirk.ue.r.appspot.com/messages?sender=${sender}`)
            setMessages(res.data)
        } else if(recipientInput.current.value){
            const res = await axios.get(`http://wedding-planner-kirk.ue.r.appspot.com/messages?recipient=${recipient}`)
            setMessages(res.data)
        } else {
            alert("Please fill out either the sender or recipient field to make a search!")
            return;
        }
        document.getElementById("messageTable").style.display = "block";
    }

    async function send(event:SyntheticEvent){
        const sender = senderInput2.current.value;
        const recipient = recipientInput2.current.value;
        const note = noteInput.current.value

        const message = {
            sender: sender,
            recipient: recipient,
            note: note
        }
        const json = JSON.stringify(message)
        try{
            await axios.post(`https://wedding-planner-kirk.ue.r.appspot.com/messages`, json, {
                headers: { 
                    'Content-Type': 'application/json'
                }
            })
            alert("Message sent successfully!")
            window.location.href = '/message'
        } catch {
            alert("An error occured. Please make sure the input fields are filled out correctly!")
        }
    }
    function returnToHome(event){
        window.location.href = '/home'
    }

    return(<div>
        <button className="left-align" onClick={returnToHome}>Return to Home</button>
        <br></br>
        <h1>Message Center</h1>
        <br></br>
        <button id="selection" onClick={composeView}>Compose new message</button>
        <button id="selection2" onClick={searchView}>Search messages</button>
        <div id="compose" hidden>
            <h5>Sender:</h5>
            <input placeholder="sender" ref={senderInput2}></input>
            <br></br>
            <h5>Recipient:</h5>
            <input placeholder="recipient" ref={recipientInput2}></input>
            <br></br>
            <textarea id="note" placeholder="message" ref={noteInput}></textarea>
            <button onClick={send}>Send</button>
            <button onClick={back}>Back</button>
        </div>


        <div id="search" hidden>
            <input placeholder="sender" ref={senderInput}></input>
            <br></br>
            <input placeholder="recipient" ref={recipientInput}></input>
            <br></br>
            <button onClick={search}>Search</button>
            <button onClick={back}>Back</button>
        </div>

        <div id="messageTable" hidden>
        {messages === undefined ? '' : <MessageTable messages={messages}></MessageTable>}
        </div>

    </div>)
}

