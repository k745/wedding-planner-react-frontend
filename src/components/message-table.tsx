
export default function MessageTable(props: any){

    const messages = props.messages;

    return(<table className="table">
        <thead><th>Sender</th><th>Recipient</th><th>Note</th><th>Timestamp</th></thead>
        <tbody>
            {messages.map(m => <tr key={m.weddingId}>
                <td>{m.sender}</td>
                <td>{m.recipient}</td>
                <td>{m.note}</td>
                <td>{new Date(m.timestamp).toString()}</td>
            </tr>)}
        </tbody>
    </table>)
}