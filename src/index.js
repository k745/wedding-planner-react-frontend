import React from 'react';
import ReactDOM from 'react-dom';
import Login from './components/login';
import Planner from './components/planner';
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import './index.css';
import Home from './components/home-page';
import Messaging from './components/messaging';

ReactDOM.render(
  <React.StrictMode>

    <Router>
      <Route exact path="/">
        <Redirect to="/login"/>
        </Route>
      <Route path="/login">
        <Login></Login>
        </Route>
        <Route path="/home">
       <Home></Home>
      </Route>
      <Route path="/message">
       <Messaging></Messaging>
      </Route>
      <Route path="/planner">
       <Planner></Planner>
      </Route>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);
